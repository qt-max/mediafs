// SPDX-License-Identifier: MIT OR GPL-2.0

#include <linux/module.h>

MODULE_AUTHOR("Maxim Mikityanskiy <maxtram95@gmail.com>");
MODULE_DESCRIPTION("Stacked filesystem that replaces UID and GID on all files");
MODULE_LICENSE("Dual MIT/GPL");
MODULE_VERSION("0.0");

static int __init mediafs_init(void)
{
	pr_info("mediafs loaded\n");
	return 0;
}

static void __exit mediafs_exit(void)
{
	pr_info("mediafs unloaded\n");
}

module_init(mediafs_init);
module_exit(mediafs_exit);
